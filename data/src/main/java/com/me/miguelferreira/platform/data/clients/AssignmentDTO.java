package com.me.miguelferreira.platform.data.clients;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AssignmentDTO {

	private long id;
	private String name;
	private RequesterDTO requester;

}
