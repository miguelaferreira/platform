package com.me.miguelferreira.platform.data.repositories;

import com.me.miguelferreira.platform.data.entities.AssignmentEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "assignment", path = "assignments")
public interface AssignmentRepository extends PagingAndSortingRepository<AssignmentEntity, Long> {
}
