package com.me.miguelferreira.platform.data.clients;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("assignmentclient")
public interface AssignmentClient {

	@RequestMapping(method = RequestMethod.GET, value="/assignment/{assignmentId}")
	AssignmentDTO getAssignmentDto(@PathVariable("assignmentId") long assignmentId);

}
