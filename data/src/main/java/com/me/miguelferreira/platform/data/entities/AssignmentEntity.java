package com.me.miguelferreira.platform.data.entities;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class AssignmentEntity extends BaseEntity {

    @Embedded
    private GeographicalLocationEntity geographicalLocationEntity = new GeographicalLocationEntity();
    @Embedded
    private AddressEntity addressEntity = new AddressEntity();
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private RequesterEntity requesterEntity;

    public GeographicalLocationEntity getGeographicalLocationEntity() {
        return geographicalLocationEntity;
    }

    public void setGeographicalLocationEntity(final GeographicalLocationEntity geographicalLocationEntity) {
        this.geographicalLocationEntity = geographicalLocationEntity;
    }

    public AddressEntity getAddressEntity() {
        return addressEntity;
    }

    public void setAddressEntity(final AddressEntity addressEntity) {
        this.addressEntity = addressEntity;
    }

    public RequesterEntity getRequesterEntity() {
        return requesterEntity;
    }

    public void setRequesterEntity(final RequesterEntity requesterEntity) {
        this.requesterEntity = requesterEntity;
    }
}
