package com.me.miguelferreira.platform.data.repositories;

import com.me.miguelferreira.platform.data.entities.ProviderEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "provider", path = "providers")
public interface ProviderRepository extends PagingAndSortingRepository<ProviderEntity, Long> {

}
