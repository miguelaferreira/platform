package com.me.miguelferreira.platform.data;

import com.me.miguelferreira.platform.data.entities.AssignmentEntity;
import com.me.miguelferreira.platform.data.entities.ProviderEntity;
import com.me.miguelferreira.platform.data.entities.RequesterEntity;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepositoryConfiguration extends RepositoryRestConfigurerAdapter {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(RequesterEntity.class, ProviderEntity.class, AssignmentEntity.class);
	}

}