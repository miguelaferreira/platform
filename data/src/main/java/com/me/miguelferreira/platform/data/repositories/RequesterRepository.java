package com.me.miguelferreira.platform.data.repositories;

import com.me.miguelferreira.platform.data.entities.RequesterEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "requester", path = "requesters")
public interface RequesterRepository extends PagingAndSortingRepository<RequesterEntity, Long> {
}
