package com.me.miguelferreira.platform.data.clients;

import lombok.Data;

@Data
public class RequesterDTO {

	private long id;
	private String name;

}
