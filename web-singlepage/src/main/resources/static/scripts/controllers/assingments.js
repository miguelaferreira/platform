angular.module('platformApp')
	.controller('AssignmentsCtrl', function ($scope, $http, $routeParams) {
		$http({
			method: 'GET',
			url: '/data/assignments/' + $routeParams.assignmentId
		}).then(function (response) {
			$scope.assignment = response.data;
		}, function(response) {
			console.error('Error requesting assignment.')
		});
	});