angular.module('platformApp')
	.controller('MainCtrl', function ($scope, $http) {
		$http({
			method: 'GET',
			url: '/data/assignments'
		}).then(function(response) {
			$scope.assignments = response.data._embedded.assignment
		}, function(response) {
			console.error('Error requesting assignments');
		});
	});