angular
	.module('platformApp', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngRoute',
		'ngSanitize',
		'ngTouch'
	])
	.config(function ($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'views/main.html',
				controller: 'MainCtrl'
			})
			.when('/assignments/:assignmentId', {
				templateUrl: 'views/assignments.html',
				controller: 'AssignmentsCtrl'
			})
			.otherwise({
				redirectTo: '/'
			});
	});