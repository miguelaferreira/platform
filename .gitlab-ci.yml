image: java:8


stages:
- build
- package
- deploy
- check
- promote


cache:
  key: all
  paths:
    - .gradle/wrapper
    - .gradle/caches


before_script:
- export GRADLE_USER_HOME=/cache/.gradle
- mkdir -p ~/.ssh && echo "${SSH_KEY}" > ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
- echo -e "Host *\n  UserKnownHostsFile /dev/null\n  StrictHostKeyChecking no\n  LogLevel quiet" >> ~/.ssh/config && chmod 600 ~/.ssh/config


build_projects:
  stage: build
  dependencies: []
  before_script:
  script:
  - ./gradlew clean build
  artifacts:
    paths:
    - '**/build/libs/**'
    - '**/build/reports/**'
    when: always
  except:
  - tags


build_tag:
  stage: build
  dependencies: []
  before_script:
  - export GRADLE_USER_HOME=/cache/.gradle
  script:
  - ./gradlew clean assemble -PreleaseBranch=${CI_BUILD_TAG}
  artifacts:
    paths:
    - '**/build/libs/**'
  only:
  - tags


ui_container:
  stage: package
  dependencies:
  - build_tag
  image: docker:latest
  before_script:
  - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} registry.gitlab.com
  script:
  - cd web-singlepage
  - cp build/libs/web-singlepage-${CI_BUILD_TAG}.jar src/main/docker
  - cd src/main/docker
  - docker build -t registry.gitlab.com/miguelaferreira/platform:${CI_BUILD_TAG}-ui .
  - docker push registry.gitlab.com/miguelaferreira/platform:${CI_BUILD_TAG}-ui
  only:
  - tags


data_container:
  stage: package
  dependencies:
  - build_tag
  image: docker:latest
  before_script:
  - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} registry.gitlab.com
  script:
  - cd data
  - cp build/libs/data-${CI_BUILD_TAG}.jar src/main/docker
  - cd src/main/docker
  - docker build -t registry.gitlab.com/miguelaferreira/platform:${CI_BUILD_TAG}-data .
  - docker push registry.gitlab.com/miguelaferreira/platform:${CI_BUILD_TAG}-data
  only:
  - tags


eureka_container:
  stage: package
  dependencies:
  - build_tag
  image: docker:latest
  before_script:
  - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} registry.gitlab.com
  script:
  - cd eureka-server
  - cp build/libs/eureka-server-${CI_BUILD_TAG}.jar src/main/docker
  - cd src/main/docker
  - docker build -t registry.gitlab.com/miguelaferreira/platform:${CI_BUILD_TAG}-eureka .
  - docker push registry.gitlab.com/miguelaferreira/platform:${CI_BUILD_TAG}-eureka
  only:
  - tags


test:
  stage: deploy
  dependencies: []
  script:
  - echo "Deployed to test"
  environment:
    name: test
    url: https://www.google.com?q=test
  only:
    - /RC-[0-9]+\.[0-9]+-.*/ # Release candidates
    - /v[0-9]+\.[0-9]+/      # Releases


acceptance:
  stage: deploy
  dependencies: []
  script:
  - echo "Deployed to acceptance"
  environment:
    name: acceptance
    url: https://www.google.com?q=acceptance
  only:
    - /v[0-9]+\.[0-9]+/      # Releases
  when: manual


production:
  stage: deploy
  dependencies: []
  script:
  - echo "Deployed to production"
  environment:
    name: production
    url: https://www.google.com?q=production
  only:
    - /v[0-9]+\.[0-9]+/      # Releases
  when: manual


functional_tests:
  stage: check
  dependencies: []
  script:
  - SECONDS=$( date +%s )
  - echo ${SECONDS} > test-report.txt
  - test "$( echo $((${SECONDS}%2)) )" -eq 0 || test ! -z "${BREAKER}" # returns 0 (true) if seconds are even, 1 (false) otherwise
  artifacts:
    paths:
    - test-report.txt
  only:
    - /RC-[0-9]+\.[0-9]+-.*/ # Release candidates
    - /v[0-9]+\.[0-9]+/      # Releases
  when: on_success


release_candidate:
  stage: promote
  dependencies: []
  script:
  - git remote set-url --push origin git@gitlab.com:miguelaferreira/platform.git
  - ./gradlew tagRelease -PreleaseBranch=${CI_BUILD_REF_NAME}
  - git push --tags
  only:
  - master
  when: on_success


final_release:
  stage: promote
  dependencies: []
  script:
  - git remote set-url --push origin git@gitlab.com:miguelaferreira/platform.git
  - ./gradlew tagRelease -PreleaseBranch=${CI_BUILD_REF_NAME}
  - git push --tags
  - git branch -d master
  - git fetch origin master:master
  - git checkout master
  - ./gradlew prepareNextVersion
  - git push origin master
  only:
  - /RC-[0-9]+\.[0-9]+-.*/ # Release candidates
  when: manual
