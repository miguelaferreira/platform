package com.me.miguelferreira.platform.model;

import lombok.Data;

@Data
public class Provider extends BaseModel {

	public Provider(long id, String name) {
		super(id, name);
	}

}
