package com.me.miguelferreira.platform.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address {

	private final String addressLine1;
	private final String addressLine2;
	private final String postalCode;
	private final String city;
	private final String country;

}
