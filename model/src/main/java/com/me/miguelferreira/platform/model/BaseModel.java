package com.me.miguelferreira.platform.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BaseModel {

	private final long id;
	private final String name;

}
